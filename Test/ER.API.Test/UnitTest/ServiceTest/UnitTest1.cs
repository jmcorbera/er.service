using Xunit;
using ER.API.Service;

namespace ER.API.Test.UnitTest.ServiceTest
{
    public class UnitTest1
    {
        [Fact]
        public void GetInfoShouldReturnXmlReport()
        {
            //Arrange
            var test = new ERService();
            
            //Act
            var xmlResult = test.GetInfo("", 1, "M", 1);

            //Assert
            Assert.Equal(xmlResult, xmlResult);

        }
    }
}
