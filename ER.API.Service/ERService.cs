﻿using System;
using System.Collections.Generic;
using System.Linq;
using ER.API.Service.Discovery;
using ER.API.Shared.Contracts;
using ER.API.Shared.Interfaces;
using ER.API.Shared.Models;

namespace ER.API.Service
{
    public class ERService : IERService
    {
        private static IEnumerable<IExternalEntity> eeImplementations;

        public ERService()
        {
            Getplugins();
        }
        
        public string GetInfo(string externalEntity, int doc, string sex, byte idApp)
        {
            string xmlQuery = String.Empty;
            IExternalEntity ee = GetEntityInstance(externalEntity);

            try
            {
                xmlQuery = GetCacheInfo(ee, doc, sex);

                if (String.IsNullOrEmpty(xmlQuery))
                {
                    xmlQuery = GetEntityInfo(ee, doc, sex, idApp);
                }         
            }
            catch (Exception ex)
            {
                xmlQuery = xmlQuery.Insert(0, "<Consulta><resultado>" + ex.Message + "</resultado></Consulta>");
            }

            return xmlQuery;
        }

        private static string GetCacheInfo(IExternalEntity ee, int doc, string sex)
        {
            string xmlQuery = String.Empty;

            // todo get info from local db 

            return xmlQuery;
        }

        private static string GetEntityInfo(IExternalEntity ee, int doc, string sex, byte idApp)
        {
            ExternalReport infoEE = ee.GetInfo(doc, sex, idApp);

            //SaveQuery(infoEE);
            int idQuery = 1; 

            return ee.GetFooterInformation(infoEE, idQuery);
        }     

        private static int SaveQuery(ExternalReport extReport)
        {
            int idQuery = 0;

            try
            {
                // Save only if return report
                if (extReport != null)
                {
                    // todo Save External report in local DB
                }
            }
            catch //(Exception ex)
            {
                //todo trace log
            }

            return idQuery;
        }   

        private static void Getplugins()
        {
            try
            {
                eeImplementations = ReflectionDiscovery.LoadPlugins<IExternalEntity>();

                if (eeImplementations == null)
                {
                    //todo trace log
                }
            }
            catch //(Exception ex)
            {
                //todo trace log
            }
        }

        private static IExternalEntity GetEntityInstance(string instance)
        {
            return eeImplementations.FirstOrDefault();
        }
    }
}
