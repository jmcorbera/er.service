using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ER.API.Service.Discovery
{
    public class ReflectionDiscovery 
    {
        // get configuration from appSettings
        private static readonly string directory = Directory.GetParent(Environment.CurrentDirectory).ToString() + "\\ER.API.Service\\Plugins";

        public static IEnumerable<T> LoadPlugins<T>()
        {
            List<T> plugins = new List<T>();
            if (Directory.Exists(directory))
            {
                var dllFiles = Directory.GetFiles(directory, "*.dll");
                foreach (var dllFile in dllFiles)
                {
                    byte[] bytes = File.ReadAllBytes(dllFile);
                    Assembly assembly = Assembly.Load(bytes);
                    var aPlugins = LoadPlugins<T>(assembly);
                    if (aPlugins != null)
                    {
                        plugins.AddRange(aPlugins);
                    }
                }
            }
            return plugins;
        }

        public static IEnumerable<T> LoadPlugins<T>(Assembly assembly)
        {
            List<T> plugins = new List<T>();
            if (assembly != null)
            {
                Type[] types = assembly.GetTypes();
                foreach (Type type in types)
                {
                    if (type.IsInterface || type.IsAbstract)
                    {
                        continue;
                    }
                    else
                    {
                        if (type.GetInterface(typeof(T).FullName) != null)
                        {
                            plugins.Add((T)Activator.CreateInstance(type));
                        }
                    }
                }
            }
            return plugins;
        }
    }
}