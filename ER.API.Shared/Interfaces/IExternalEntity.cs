using ER.API.Shared.Models;

namespace ER.API.Shared.Interfaces
{
    public interface IExternalEntity
    {
        ExternalReport GetInfo(int doc, string sex, byte idApp);

        bool CheckValidDate(ExternalReport externalReport, out string xml);

        string GetEntityName();

        int GetEntityId();

        string GetFooterInformation(ExternalReport er, int idQuery);
    }
}