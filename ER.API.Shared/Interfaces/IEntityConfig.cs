using ER.API.Shared.Models;

namespace ER.API.Shared.Interfaces
{
    public interface IEntityConfig
    {
        EEParameter GetConfig();

        void SetConfig(byte id, string Name, short ValidityQuery);
    }
}