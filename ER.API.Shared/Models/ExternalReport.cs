using System;

namespace ER.API.Shared.Models
{
    public class ExternalReport
    {
        public int Id { get; set; }
        public byte IdExternalEntity { get; set; } 
        public byte IdAplication { get; set; }   
        public string XmlReport { get; set; }  
        public string ReportResult { get; set; }    
        public string ReportReference { get; set; } 
        public short QueryValidity { get; set; }    
        public DateTime CreatedDate { get; set; }   
        public int IdPerson { get; set; }  
        public string FirstName { get; set; } 
        public string LaststName { get; set; } 
        public int Doc { get; set; } 
        public string Sex { get; set; } 
        public string CUIL { get; set; } 
    }
}