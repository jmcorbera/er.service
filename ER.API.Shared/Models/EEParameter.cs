namespace ER.API.Shared.Models
{
    public class EEParameter
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public short ValidityQuery { get; set; }
        public string StringParams { get; set; }
    }
}