namespace ER.API.Shared.Contracts
{
    public interface IERService
    {
         string GetInfo(string externalEntity, int doc, string sex, byte idApp);
    }
}