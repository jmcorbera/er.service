﻿using System.Linq;
using System.Xml.Linq;
using ER.API.PluginImplementation.Config;
using ER.API.PluginImplementation.Report;
using ER.API.Shared.Interfaces;
using ER.API.Shared.Models;

namespace ER.API.PluginImplementation
{
    public class PlugInReport : IExternalEntity
    {
        private ReportConfig reportConfig;

        public PlugInReport()
        {
            reportConfig = new ReportConfig();
            reportConfig.SetConfig(1, "Report Online", 20);
        }

        public ExternalReport GetInfo(int doc, string sex, byte idApp)
        {
            string jsonQuery = "";
            return new APIReport().GetRolReport(jsonQuery, reportConfig, idApp);
        }

        public bool CheckValidDate(ExternalReport externalReport, out string xml)
        {
            xml = null;
            return true;
        }

        public int GetEntityId()
        {
            return reportConfig.Id;
        }

        public string GetEntityName()
        {
            return reportConfig.Name;
        }

        public string GetFooterInformation(ExternalReport er, int idQuery)
        {
            try
            {
                XElement xElement = XElement.Parse(er.XmlReport);

                xElement.Descendants("informe").Elements("id").First().AddAfterSelf(new XElement("idConsulta", idQuery.ToString()));
                xElement.Descendants("informe").Elements("id").First().AddAfterSelf(new XElement("nombreEntidad", "Reporte Online"));
                xElement.Descendants("informe").Elements("id").First().AddAfterSelf(new XElement("resultado", er.ReportResult));

                return xElement.ToString();
            }
            catch  //(Exception ex)
            {
            }

            return er.XmlReport.ToString();
        }
    }
}
