using ER.API.Shared.Interfaces;
using ER.API.Shared.Models;

namespace ER.API.PluginImplementation.Config
{
    public class ReportConfig : IEntityConfig
    {
        #region Private Variables

        protected static readonly string URL_REPORT = "";
        protected static readonly string USER = "";
        protected static readonly string PASS = "";
        protected EEParameter ee;

        #endregion

        #region Properties

        public string Url_Report
        {
            get
            {
                return URL_REPORT;
            }
        }

        public string User
        {
            get
            {
                return USER;
            }
        }

        public string Password
        {
            get
            {
                return PASS;
            }
        }

        public short ValidityQuery
        {
            get
            {
                return ee.ValidityQuery;
            }
        }

        public byte Id
        {
            get
            {
                return ee.Id;
            }
        }

        public string Name
        {
            get
            {
                return ee.Name;
            }
        }

        public EEParameter GetConfig()
        {
            return ee;
        }

        public void SetConfig(byte id, string name, short validityQuery)
        {
            if (ee == null)
                ee = new EEParameter();

            this.ee.Id = id;
            this.ee.Name = name;
            this.ee.ValidityQuery = validityQuery;
        }

        #endregion
    }
}