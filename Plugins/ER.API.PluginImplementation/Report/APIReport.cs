using System;
using System.Xml.Linq;
using ER.API.PluginImplementation.Config;
using ER.API.Shared.Models;

namespace ER.API.PluginImplementation.Report
{
    public class APIReport
    {
        public ExternalReport GetRolReport(string jsonQuery, ReportConfig rolConfig, byte idApp)
        {
            try
            {                
                return GenerateReport(rolConfig, idApp);
            }
            catch //(Exception ex)
            {
                throw new Exception("Error de Datos");
            }
        }

        private ExternalReport GenerateReport(ReportConfig rolConfig, byte idApp)
        {

            try
            {
                ExternalReport er = new ExternalReport();

                er.Doc = 6084960;
                er.FirstName = "Pepe Argento";
                er.XmlReport = XElement.Parse("<informe><id>999</id></informe>").ToString();
                return er;
            }
            catch //(Exception ex)
            {
                //Trace.Error(string.Format("Error ROL Generate Report : {0}", ex.Message));
            }

            return null;
        }
    }
}