﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ER.API.Shared.Contracts;

namespace ER.API.Rest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ERServiceController : ControllerBase, IERService
    {
        private readonly ILogger<ERServiceController> _logger;
        private readonly IERService _service;

        public ERServiceController(ILogger<ERServiceController> logger, IERService service)
        {
            _logger = logger;
            this._service = service;
        }

        [HttpPost]
        public string GetInfo(string externalEntity, int doc, string sex, byte idApp)
        {
            return this._service.GetInfo(externalEntity, doc, sex, idApp);
        }
    }
}
